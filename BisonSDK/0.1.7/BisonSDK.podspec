#
# Be sure to run `pod lib lint BisonSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BisonSDK'
  s.version          = '0.1.7'
  s.summary          = 'BisonSDK is an iOS Library to help the implementation of uploading and deleting file in Bison Storage.'

  s.homepage         = 'http://www.syllistudio.com'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Sylli studio' => 'sirichai@syllistudio.com' }
  s.source           = { :git => 'https://bitbucket.org/syllistudio/bisoniossdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'BisonSDK/Classes/**/*'
  
 #s.resource_bundles = {
 #'BisonSDK' => ['BisonSDK/Assets/*.png']
 #}

  # s.public_header_files = 'Pod/Classes/**/*.h'
    s.frameworks = 'UIKit', 'MapKit'
    s.dependency 'Alamofire', '4.6.0'
    s.dependency 'AlamofireObjectMapper', '~> 5.0.0'
end
